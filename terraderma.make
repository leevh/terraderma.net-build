; ----------------
; Generated makefile from http://drushmake.me
; Permanent URL: http://drushmake.me/file.php?token=1994acf24837
; ----------------
;
; This is a working makefile - try it! Any line starting with a `;` is a comment.
  
; Core version
; ------------
; Each makefile should begin by declaring the core version of Drupal that all
; projects should be compatible with.
  
core = 7.x
  
; API version
; ------------
; Every makefile needs to declare its Drush Make API version. This version of
; drush make uses API version `2`.
  
api = 2
  
; Core project
; ------------
; In order for your makefile to generate a full Drupal site, you must include
; a core project. This is usually Drupal core, but you can also specify
; alternative core projects like Pressflow. Note that makefiles included with
; install profiles *should not* include a core project.
  
; Drupal 7.x. Requires the `core` property to be set to 7.x.
projects[drupal][version] = 7

  
  
; Modules
; --------


projects[admin_menu][version] = "3"
projects[ais][version] = "1"
projects[ctools][version] = "1"
projects[context][version] = "3"
projects[css3pie][version] = "2"
projects[delta][version] = "3"
projects[ds][version] = "1"
projects[media][version] = "1"
projects[filefield_sources][version] = "1"
projects[fontyourface][version] = "2"
projects[imce][version] = "1"
projects[imce_wysiwyg][version] = "1"
projects[libraries][version] = "1"
projects[link][version] = "1"
projects[menu_block][version] = "2"
projects[omega_tools][version] = "3"
projects[page_title][version] = "2"
projects[pathauto][version] = "1"
projects[seo_checklist][version] = "4"
projects[token][version] = "1"
projects[views][version] = "3"
projects[views_slideshow][version] = "3"
projects[webform][version] = "3"
projects[weight][version] = "2"
projects[wysiwyg][version] = "2"
projects[colorbox][version] = "1"
projects[mollom][version] = "2"

; FLEXSLIDER IS CALLING ITS OWN MAKEFILE WHICH I COULDNT DO.  DO THIS UNTIL 2 BRANCH IS OUT - THEN UPGRADE AND TEST
projects[flexslider][download][type] = "git"
projects[flexslider][download][url] = "git@bitbucket.org:leevh/flexslider-7.x-1.0-rc3.git"
projects[flexslider][type] = "module"



  

; Themes
; --------
projects[] = omega


projects[terraderma][download][type] = "git"
projects[terraderma][download][url] = "git@bitbucket.org:leevh/terraderma-d7-theme.git"
projects[terraderma][type] = "theme"
projects[terraderma][download][branch] = "prod"


  
  
; Libraries
; ---------

; Please fill the following out. Type may be one of get, cvs, git, bzr or svn,
; and url is the url of the download.
libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.4/ckeditor_3.6.4.zip"
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][type] = "library"

; Please fill the following out. Type may be one of get, cvs, git, bzr or svn,
; and url is the url of the download.
libraries[ckfinder][download][type] = "get"
libraries[ckfinder][download][url] = "http://download.cksource.com/CKFinder/CKFinder%20for%20PHP/2.2.2.1/ckfinder_php_2.2.2.1.zip"
libraries[ckfinder][directory_name] = "ckfinder"
libraries[ckfinder][type] = "library"

; Please fill the following out. Type may be one of get, cvs, git, bzr or svn,
; and url is the url of the download.
libraries[PIE][download][type] = "get"
libraries[PIE][download][url] = "http://css3pie.com/download-latest"
libraries[PIE][directory_name] = "PIE"
libraries[PIE][type] = "library"

;REMOVE THIS WHEN UPDATING TO NEW FLEXSLIDER MODULE, IT HAS ITS OWN MAKE FILE FOR THIS.

libraries[flexslider][download][type] = "git"
libraries[flexslider][download][url] = "git@bitbucket.org:leevh/flexslider-1.8.git"
libraries[flexslider][directory_name] = "flexslider"
libraries[flexslider][type] = "library"


;COLORBOX IS IN MY REPO FOR NOW, ITS GIT REPO WAS NOT INCLUDING THE MIN.JS FILE

libraries[colorbox][download][type] = "git"
libraries[colorbox][download][url] = "git@bitbucket.org:leevh/colorbox.git"
libraries[colorbox][directory_name] = "colorbox"
libraries[colorbox][type] = "library"

; No libraries were included



